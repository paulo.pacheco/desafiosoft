import requests
from bs4 import BeautifulSoup

#cria sessao pra ficar logado
session = requests.session()
#chama a pagina de login
request = session.get('https://gitlab.com/users/sign_in')
#transoforma em texto
soup = BeautifulSoup(request.content, 'html5lib')
#acha o token
token = soup.find('input', {'name': 'authenticity_token'})['value']

#monta o header pra login
data = {'authenticity_token': token, 'user[login]': 'pihov', 'user[password]': 'aj8XkcEH59rPLMMg'}
#manda de volta o header do login para a pagina
request = session.post('https://gitlab.com/users/sign_in', data=data)
#pega a pagina dos commits
request = session.get('https://gitlab.com/softplan/requests/commits/master')
#converte em texto
soup = BeautifulSoup(request.content, 'html5lib')
print(soup.prettify())
#comeca na pagina 0
offset = 0
#enquanto funcionar continua
while request.status_code ==200:

    #faz pagina por pagina
    request = session.get("https://gitlab.com/softplan/requests/commits/"
                          "master?limit=40&offset=" +str(offset))

    soup = BeautifulSoup(request.content, 'html5lib')
    #acha no texto a linha do commit
    for link in soup.findAll('li', {'class':'commit flex-row js-toggle-container'}):
        #no href nem todos funcionam, ai tem que fazer assim
        for link2 in link.find('a'):
            usu = link2.get('title')
        data = link.find('time', {'class':'js-timeago'})['title']
        #o que aparece é parcial, o completo ta no clipboard
        hash = link.find('button', {'class': 'btn btn btn-default'})['data-clipboard-text']

        #exibi o que pegou
        print(' Usuario: '+str(usu)+' Data: '+str(data)+' hash: '+str(hash))

    #soma pra ir pros proximos 40
    offset = offset + 40